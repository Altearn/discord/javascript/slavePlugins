const { MessageEmbed } = require("discord.js");
const { readdir } = require("fs");
module.exports.run = [
    main = async (client, message, args) => {
        let embed = new MessageEmbed()
            .setColor("#f28028");
        
        const showAll = !args[0] || args[0].toLowerCase() === 'all';
        const { plugins : pluginsAll, config } = client;

        if (showAll) {
            let text = "**__Voici les differents plugin :__**\n";
            embed.setTitle("PLUGIN")
                .setFooter("Use plugin <plugin> for have more information | by Altearn")

            await pluginsAll.forEach(plugin => {
                //const plug = require("../../../plugins/" + plugin + "/plugin.json")
                text += `\n**・ ${plugin.name} :** (${plugin.tag})\n${plugin.description}\n`
            })
            embed.setDescription(text);
            message.channel.send(embed).catch(err => err)
        } else {
            try {
                const plug = require(`../../../plugins/${args[0]}/plugin.json`)
                //------ PENSEZ A METTRE DES INFO SUP ------
                //comme la version du plugin ses dépendances ect...
                //------ PENSEZ A METTRE DES INFO SUP ------
            } catch {
                message.reply(" le plugin n'existe pas ou alors je n'ai pas réussi a recupérer les informations de ce plugin");
            }
        }
    }
];

module.exports.help = {
    name: "plugin",
    desc: "plugin",
    aliases: ["plg"],
    usage: "[plugin]"
}