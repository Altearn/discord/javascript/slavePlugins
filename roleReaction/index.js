module.exports.run = client => {
    const FileSync = require('lowdb/adapters/FileSync')
    const low = require('lowdb')
    const adapters = new FileSync(`../config.json`);
    const db = low(adapters);
    db.defaults({ reactionRole: [] }).write();
}